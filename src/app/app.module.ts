import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule, JsonpModule } from '@angular/http';
import { RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { ShirtListComponent } from './components/shirt-list/shirt-list.component';
import { HomeComponent } from './components/home/home.component';
import { ShirtDetailsComponent } from './components/shirt-details/shirt-details.component';

@NgModule({
    declarations: [
        AppComponent,
        HeaderComponent,
        FooterComponent,
        HomeComponent,
        ShirtListComponent,
        ShirtDetailsComponent
    ],
    imports: [
        BrowserModule,
        FormsModule,
        HttpModule,
        JsonpModule,
        RouterModule.forRoot([
            {
                path: '',
                redirectTo: '/home',
                pathMatch: 'full'
            },
            {
                path: 'home',
                component: HomeComponent
            },
            {
                path: 'details/:id',
                component: ShirtDetailsComponent
            }
        ])
    ],
    providers: [],
    bootstrap: [AppComponent]
})

export class AppModule { }
