import { TestBed, inject, async } from '@angular/core/testing';
import { HttpModule, Http, BaseRequestOptions, XHRBackend, Response, ResponseOptions } from '@angular/http';
import { MockBackend } from '@angular/http/testing';
import { MockAppConfig } from './config.mock';

import { ShirtsService } from './shirts.service';

import { AppConfig } from '../../app.config';

describe('ShirtsService', () => {

    const mockResponse = [
        { id: 0, name: 'Some shirt' },
        { id: 1, name: 'Some other shirt' }
    ];

    beforeEach(() => {

        TestBed.configureTestingModule({
            imports: [ HttpModule ],
            providers: [
                ShirtsService,
                { provide: XHRBackend, useClass: MockBackend }
            ]
        });

    });

    it('should have the correct methods',
        inject([ShirtsService], (shirtsService: ShirtsService) => {

            expect(shirtsService.getShirts).toBeDefined();
            expect(shirtsService.getShirt).toBeDefined();

        })
    )

    it('should call the correct endpoint using app config',

        inject([ShirtsService, XHRBackend], (shirtsService: ShirtsService, mockBackend) => {

            // `mockRespond` emits synchronously, neat!
            mockBackend.connections.subscribe((connection) => {
                if (connection.request.url !== `${AppConfig.API_ROOT}/shirts`) {
                    connection.mockError();
                } else {
                    connection.mockRespond(new Response(new ResponseOptions({
                        body: JSON.stringify(mockResponse)
                    })));
                }
            });

            shirtsService
                .getShirts()
                .subscribe( () => { /* should pass */ }, error => fail() );

        }))

    it('should return all shirts via observables',

        inject([ShirtsService, XHRBackend], (shirtsService: ShirtsService, mockBackend) => {

            // `mockRespond` emits synchronously, neat!
            mockBackend.connections.subscribe((connection) => {
                connection.mockRespond(new Response(new ResponseOptions({
                    body: JSON.stringify(mockResponse)
                })));
            });

            shirtsService
                .getShirts()
                .subscribe((shirts) => {
                    expect(shirts.length).toBe(2);
                });

        }))

    it('should retun a single shirt via promise',

        inject([ShirtsService, XHRBackend], (shirtsService: ShirtsService, mockBackend) => {

            mockBackend.connections.subscribe((connection) => {
                connection.mockRespond(new Response(new ResponseOptions({
                    body: JSON.stringify(mockResponse)
                })));
            });

            const expected = mockResponse[1];

            shirtsService
                .getShirt(1)
                .then((subject) => {
                    expect(subject).toEqual(expected);
                });

        })
    )
});
