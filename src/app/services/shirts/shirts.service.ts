import { Injectable, Inject } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/observable/throw';

import { Shirt } from '../../models/shirt';
import { AppConfig } from '../../app.config';

@Injectable()
export class ShirtsService {

    private root: String = AppConfig.API_ROOT;

    constructor(private http: Http) { }

    getShirts (): Observable<Shirt[]> {
        const url = `${this.root}/shirts`;
        return this
            .http
            .get(url)
            .map(this.extractData)
            .catch((error: any) => Observable.throw(`Failed to fetch shirts, reason: ${error.json().error}`));
    }

    getShirt (id: number): Promise<Shirt> {
        return this
            .getShirts()
            .toPromise()
            .then(heroes => heroes.find(hero => hero.id === id));
    }

    private extractData (res: Response): Shirt {
        return res.json();
    }

}
