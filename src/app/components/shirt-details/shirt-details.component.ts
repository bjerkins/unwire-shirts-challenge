import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { ShirtsService } from '../../services/shirts/shirts.service';
import { Shirt } from '../../models/shirt';

import 'rxjs/add/operator/switchMap';

@Component({
    selector: 'app-shirt-details',
    templateUrl: './shirt-details.component.html',
    styleUrls: ['./shirt-details.component.scss'],
    providers: [ShirtsService]
})
export class ShirtDetailsComponent implements OnInit {

    shirt: Shirt;
    isLoading: Boolean = true;
    error: any;

    constructor(
        private shirtsService: ShirtsService,
        private route: ActivatedRoute
    ) { }

    ngOnInit() {
        this.route.params
            .switchMap((params: Params) => this.shirtsService.getShirt(+params['id']))
            .subscribe((shirt: Shirt) => {
                this.shirt = shirt
                this.isLoading = false;
            }, error => this.error = error);
    }

}
