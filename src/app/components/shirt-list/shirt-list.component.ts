import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { Shirt } from '../../models/shirt';
import { FormsModule } from '@angular/forms';

@Component({
    selector: 'app-shirt-list',
    templateUrl: './shirt-list.component.html',
    styleUrls: ['./shirt-list.component.scss']
})
export class ShirtListComponent implements OnInit {

    filter: Object = {
        color: '',
        size: ''
    };

    filteredShirts: Shirt[];
    _shirts: Shirt[];

    @Input('shirts')
    set shirts(value: Shirt[]) {
        this._shirts = value;
        this.filteredShirts = value;
    }

    constructor(private router:Router) { }

    ngOnInit() {
    }

    applyFilter(color, size) {
        this.filteredShirts = this._shirts.filter((shirt: Shirt) => {
            return shirt.colour.startsWith(color) && shirt.size.startsWith(size);
        });
    }

    navigateToShirt(shirt: Shirt) {
        this.router.navigate(['/details', shirt.id]);
    }

}
