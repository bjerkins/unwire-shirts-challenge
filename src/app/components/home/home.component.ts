import { Component, OnInit } from '@angular/core';
import { ShirtsService } from '../../services/shirts/shirts.service';
import { Shirt } from '../../models/shirt';

@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.scss'],
    providers: [ ShirtsService ]
})
export class HomeComponent implements OnInit {

    shirts: Shirt[];
    error: any;

    constructor(private shirtsService: ShirtsService) { }

    ngOnInit() {
        this
            .shirtsService
            .getShirts()
            .subscribe((shirts: Shirt[]) => {
                this.shirts = shirts
            }, error => this.error = error);
    }

}
