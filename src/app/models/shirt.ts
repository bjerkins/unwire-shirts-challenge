export class Shirt {
    constructor(
        public id: number,
        public price: number,
        public picture: string,
        public colour: string,
        public size: string,
        public name: string
    ) {}
}
